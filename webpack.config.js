const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({

  name: 'fe-persee-gestion-taches',

  exposes: {
    './Module' : './src/app/components/gestionTaches.module.ts',
    MenuEntry : './src/app/components/gestion-tache-menu.json'
  },

  shared: {
    ...shareAll({ singleton: true, strictVersion: true, requiredVersion: 'auto' }),
  },

});
